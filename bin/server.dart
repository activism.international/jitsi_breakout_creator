import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:args/args.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;

// For Google Cloud Run, set _hostname to '0.0.0.0'.
const _hostname = '0.0.0.0';

String jitsiHost = 'meet.activism.international';
int peoplePerBR = 3;

List<Conference> conferences = [];

void main(List<String> args) async {
  var parser = ArgParser()..addOption('port', abbr: 'p');
  var result = parser.parse(args);

  // For Google Cloud Run, we respect the PORT environment variable
  var portStr = result['port'] ?? Platform.environment['PORT'] ?? '8080';
  var port = int.tryParse(portStr);

  if (port == null) {
    stdout.writeln('Could not parse port value "$portStr" into a number.');
    // 64: command line usage error
    exitCode = 64;
    return;
  }

  var handler = const shelf.Pipeline()
      .addMiddleware(shelf.logRequests())
      .addHandler(_echoRequest);

  var server = await io.serve(handler, _hostname, port);
  print('Serving at http://${server.address.host}:${server.port}');
}

String generateSpecialRoomString(String id, List<String> rooms) {
  var result = '';
  if (rooms != []) {
    for (var room in rooms) {
      result +=
      "links.innerHTML += \"<li>${room} Room: Join by <a href='/join_${room}/${id}'>link</a></li>\"\;\n";
    }
    return result;
  }
}

shelf.Response _echoRequest(shelf.Request request) {
  if (request.url.hasEmptyPath) {
    return shelf.Response.ok('''
<html>
<body>
<h3>Create Jitsi Breakout Rooms</h3>
<form method="get" action="/create/">
<input name="conferenceName" type="name" placeholder="Conference name" required />
<input name="peoplePerBR" type="number" value="3" min="2" max="20" placeholder="People ber breakout room" required />
<input name="specialRooms" type="text" placeholder="add comma-separated rooms (e.g. BiPOC only, FINTA* only, ..." />
<input name="jitsiHost" type="text" value="meet.activism.international" placeholder="Jitsi host" required />
<input type="submit">
</form>
</body>
</html>
''', headers: {'content-type': 'text/html; charset=utf-8'});
  }
  if (!request.url.hasEmptyPath &&
      request.url.pathSegments[0].contains('join') &&
      request.url.pathSegments[1] != null) {
    final conf = conferences
        .firstWhere((element) => element.id == request.url.pathSegments[1]);
    String roomName = request.url.pathSegments[0].split('_')[1];
    var roomBr = conf.addUser(roomName);
    var target = Uri(
        scheme: 'https',
        host: conf.host,
        path: '${conf.name}_${roomName}_${conf.id}$roomBr');
    return shelf.Response.found(target.toString());
  }
  if (!request.url.hasEmptyPath &&
      request.url.pathSegments[0] == 'show' &&
      request.url.pathSegments[1] != null) {
    final conf = conferences
        .firstWhere((element) => element.id == request.url.pathSegments[1]);
    final specialRoomString = generateSpecialRoomString(conf.id, conf.specialRoomNames);
    return shelf.Response.ok('''
<html>
<body>
<h3>Conference: ${conf.name}</h3>
<ul id="linkGenerator"></ul>
<ul>
<li>People per breakout room: ${conf.peoplePerBR}</li>
<li>Jitsi host: <a href="https://${conf.host}/">${conf.host}</a></li>
</ul>
<script>
  var links = document.getElementById("linkGenerator");
  links.innerHTML = "<li>Rooms for all: Join by <a href='/join_all/${conf.id}'>link</a></li>";
  ${specialRoomString}
</script>
</body>
</html>
''', headers: {'content-type': 'text/html; charset=utf-8'});
  }
  if (!request.url.hasEmptyPath && request.url.pathSegments[0] == 'create') {
    final host = request.url.queryParameters['jitsiHost'] ?? jitsiHost;
    final peoplePerBR =
        int.tryParse(request.url.queryParameters['peoplePerBR'].toString()) ??
            3;
    final name =
        request.url.queryParameters['conferenceName'] ?? getRandString(5);
    List<String> specialRooms = [];
    if (request.url.queryParameters['specialRooms'].length > 0) {
      specialRooms = request.url.queryParameters['specialRooms'].split(
          ',');
      specialRooms.forEach((room) => room.trim);
    }
    var createdConference = Conference(
        host: host,
        peoplePerBR: peoplePerBR,
        name: name,
        specialRoomNames: specialRooms);
    conferences.add(createdConference);
    return shelf.Response.found('/show/${createdConference.id}');
  }
  return shelf.Response.notFound('Not found.');
}

class Conference {
  final String host;
  final String name;
  String standardRoom = 'all';
  int peoplePerBR;
  final List<String> specialRoomNames;
  int currentBr;
  String id;
  List<Rooms> rooms = [];

  Conference({this.specialRoomNames, this.host, this.name, this.peoplePerBR}) {
    currentBr = 0;
    id = getRandString(6);
    var stdRoom = Rooms(name: standardRoom, roomCurrentBr: currentBr);
    rooms.add(stdRoom);
    if (specialRoomNames != []) {
      print(specialRoomNames.length);
      print(specialRoomNames);
      for (var speRoomNames in specialRoomNames) {
        currentBr ++;
        var speRoom = Rooms(name: speRoomNames, roomCurrentBr: currentBr);
        rooms.add(speRoom);
      }
    }
  }

  int addUser(String roomName){
    var room = rooms.firstWhere((element) => element.name == roomName);
    var roomOccupation = room.roomsAndParticipants[room.roomCurrentBr];
    if (roomOccupation == peoplePerBR){
      currentBr += 1;
      room.roomCurrentBr = currentBr;
      room.roomsAndParticipants[room.roomCurrentBr] = 1;
    }
    else {
      room.roomsAndParticipants[room.roomCurrentBr] += 1;
    }
    return room.roomCurrentBr;
  }
}

class Rooms {
  String name;
  Map<int, int> roomsAndParticipants = {}; //room 1, 3 participants, room2, 2 participants, ...
  int roomCurrentBr;

  Rooms({this.name, this.roomCurrentBr}){
    roomsAndParticipants[roomCurrentBr] = 0;
  }
}

String getRandString(int len) {
  var random = Random.secure();
  var values = List<int>.generate(len, (i) => random.nextInt(255));
  return base64UrlEncode(values);
}